<?php

namespace App\Infrastructure\Security\Exception;

class RoleNotFoundException extends \Exception
{
}