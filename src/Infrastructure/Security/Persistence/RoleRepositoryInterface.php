<?php

namespace App\Infrastructure\Security\Persistence;

interface RoleRepositoryInterface
{
    /**
     * @return string[]
     */
    public function getRolesForActivity(string $permission): array;
}