<?php

namespace App\Infrastructure\Security\Persistence\InMemeory;

use App\Infrastructure\Security\Exception\RoleNotFoundException;
use App\Infrastructure\Security\Persistence\RoleRepositoryInterface;

class InMemoryRoleRepository implements RoleRepositoryInterface
{
    /**
     * @return array<string, array<string>>
     */
    private function roles(): array
    {
        return [
            'someMethod' => ['ROLE_READER, ROLE_READER_VIP']
        ];
    }

    /**
     * @return string[]
     * @throws RoleNotFoundException
     */
    public function getRolesForActivity(string $permission): array
    {
        $roles = $this->roles();

        if (array_key_exists($permission, $roles)) {
            return $roles[$permission];
        }

        throw new RoleNotFoundException('Role not found');
    }
}