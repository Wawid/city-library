<?php

namespace App\Infrastructure\Security\Service;

use App\Infrastructure\Security\Persistence\RoleRepositoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AuthorizationService
{
    private RoleRepositoryInterface $roleRepository;
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(
        RoleRepositoryInterface $roleRepository,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->roleRepository = $roleRepository;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function authorize(string $action): bool
    {
        $roles = $this->roleRepository->getRolesForActivity($action);

        foreach ($roles as $role) {
            if ($this->authorizationChecker->isGranted($role) === false) {
                throw new AccessDeniedException('Unable to access this page!');
            }
        }

        return true;
    }
}