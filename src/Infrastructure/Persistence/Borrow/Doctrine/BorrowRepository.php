<?php

namespace App\Infrastructure\Persistence\Borrow\Doctrine;

use App\Domain\Borrow\ReadModel\Entity\Borrow;
use App\Domain\Borrow\ReadModel\Persistence\BorrowRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class BorrowRepository implements BorrowRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Borrow $borrow): void
    {
        $this->entityManager->persist($borrow);
        $this->entityManager->flush();
    }

    public function remove(Borrow $borrow): void
    {
        // TODO: Implement remove() method.
    }

    public function find(string $id): ?Borrow
    {
        return null;
    }
}