<?php

namespace App\Infrastructure\Persistence\Reader\Doctrine;

use App\Domain\Reader\ReadModel\Entity\Reader;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class ReaderRepository implements ReaderRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Reader $reader): void
    {
        $this->entityManager->persist($reader);
        $this->entityManager->flush();
    }

    public function remove(Reader $reader): void
    {
        $this->entityManager->remove($reader);
        $this->entityManager->flush();

    }

    public function find(string $id): ?Reader
    {
       return  $this->entityManager->find(Reader::class, $id);
    }
}