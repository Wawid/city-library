<?php

namespace App\Infrastructure\Persistence\Book\Doctrine;

use App\Domain\Book\ReadModel\Entity\Book;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class BookRepository implements BookRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Book $book): void
    {
        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }

    public function remove(Book $book): void
    {
        $this->entityManager->remove($book);
        $this->entityManager->flush();
    }

    public function find(string $id): ?Book
    {
        return $this->entityManager->find(Book::class, $id);
    }
}