<?php

namespace App\Infrastructure\Persistence\IsbnRegistry\Doctrine;

use App\Domain\Book\Registry\Entity\IsbnRegistry;
use App\Domain\Book\Registry\Persistence\IsbnRegistryRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class IsbnRegistryRepository implements IsbnRegistryRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(IsbnRegistry $isbnRegistry): void
    {
        $this->entityManager->persist($isbnRegistry);
        $this->entityManager->flush();
    }

    public function findByIsbn(string $isbn): ?IsbnRegistry
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('e')
            ->from(IsbnRegistry::class, 'e')
            ->where('e.isbn = :isbn')
            ->setParameter('isbn', $isbn);

        return $query->getQuery()->getOneOrNullResult();
    }
}