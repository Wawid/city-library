<?php

namespace App\Infrastructure\Persistence\IsbnRegistry\InMemory;

use App\Domain\Book\Registry\Entity\IsbnRegistry;
use App\Domain\Book\Registry\Persistence\IsbnRegistryRepositoryInterface;

class IsbnRegistryRepository implements IsbnRegistryRepositoryInterface
{
    /**
     * @var array<string, IsbnRegistry>
     */
    private array $isbnRegistries = [];

    public function save(IsbnRegistry $isbnRegistry): void
    {
        $this->isbnRegistries[$isbnRegistry->getId()] = $isbnRegistry;
    }

    public function findByIsbn(string $isbn): ?IsbnRegistry
    {
        /** @var IsbnRegistry $isbnRegistry */
        foreach ($this->isbnRegistries as $isbnRegistry) {
            if ($isbnRegistry->getIsbn() === $isbn) {
                return $this->isbnRegistries[$isbnRegistry->getId()];
            }
        }

        return null;
    }
}