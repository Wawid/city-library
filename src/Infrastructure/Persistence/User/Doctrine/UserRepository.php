<?php

namespace App\Infrastructure\Persistence\User\Doctrine;

use App\Domain\User\ReadModel\Entity\User;
use App\Domain\User\ReadModel\Persistence\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserRepository implements UserRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function remove(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    public function find(string $id): ?User
    {
        return $this->entityManager->find(User::class, $id);
    }

    private function findByEmail(string $email): ?User
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('e')
            ->from(User::class, 'e')
            ->where('e.email = :email')
            ->setParameter('email', $email);

        return $query->getQuery()->getOneOrNullResult();
    }
}