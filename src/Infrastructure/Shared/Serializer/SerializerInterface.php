<?php

namespace App\Infrastructure\Shared\Serializer;

interface SerializerInterface
{
    public function serialize(object $object): string;

    public function deserialize(string $object, string $type): object;
}