<?php

namespace App\Infrastructure\Shared\Serializer;

use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class JsonSerializer implements SerializerInterface
{
    private SymfonySerializerInterface $serializer;
    private const JSON = 'json';

    public function __construct(SymfonySerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function serialize(object $object): string
    {
        return $this->serializer->serialize($object, self::JSON);
    }

    public function deserialize(string $object, string $type): object
    {
        return $this->serializer->deserialize($object, $type, self::JSON);
    }
}