<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="src/Infrastructure/Shared/Bus/Snapshot/Persist/DoctrineSnapshotStorageRepository")
 */
class SnapshotEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $aggregateId;

    /**
     * @ORM\Column(type="string")
     */
    private string $aggregateClass;

    /**
     * @ORM\Column(type="string")
     */
    private string $aggregateObject;

    /**
     * @ORM\Column(type="integer")
     */
    private int $lastVersion;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdDate;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function setAggregateId(string $aggregateId): void
    {
        $this->aggregateId = $aggregateId;
    }

    public function getAggregateClass(): string
    {
        return $this->aggregateClass;
    }

    public function setAggregateClass(string $aggregateClass): void
    {
        $this->aggregateClass = $aggregateClass;
    }

    public function getAggregateObject(): string
    {
        return $this->aggregateObject;
    }

    public function setAggregateObject(string $aggregateObject): void
    {
        $this->aggregateObject = $aggregateObject;
    }

    public function getLastVersion(): int
    {
        return $this->lastVersion;
    }

    public function setLastVersion(int $lastVersion): void
    {
        $this->lastVersion = $lastVersion;
    }

    public function getCreatedDate(): DateTimeImmutable
    {
        return $this->createdDate;
    }

    public function setCreatedDate(DateTimeImmutable $createdDate): void
    {
        $this->createdDate = $createdDate;
    }
}