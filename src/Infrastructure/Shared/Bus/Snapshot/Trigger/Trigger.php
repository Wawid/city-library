<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Trigger;

use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;

interface Trigger
{
    public function shouldCreateSnapshot(AggregateRootInterface $aggregateRoot): bool;
}