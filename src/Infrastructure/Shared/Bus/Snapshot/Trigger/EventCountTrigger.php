<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Trigger;

use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;

class EventCountTrigger implements Trigger
{
    private int $eventCount = 30;

    public function shouldCreateSnapshot(AggregateRootInterface $aggregateRoot): bool
    {
        $clonedAggregateRoot = clone $aggregateRoot;

        foreach ($clonedAggregateRoot->pullDomainEvents() as $domainMessage) {
            if (($domainMessage->getVersion()) % $this->eventCount === 0) {
                return true;
            }
        }

        return false;
    }
}