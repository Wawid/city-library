<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Factory;

use App\Infrastructure\Shared\Bus\Snapshot\Snapshot;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;

class SnapshotFactory
{
    public static function createFromAggregate(AggregateRootInterface $aggregateRoot): Snapshot
    {
        return new Snapshot(
            get_class($aggregateRoot),
            $aggregateRoot->getAggregateId()->getValue(),
            $aggregateRoot,
            $aggregateRoot->getVersion(),
            new \DateTimeImmutable('now', new \DateTimeZone('Europe/Warsaw'))
        );
    }
}