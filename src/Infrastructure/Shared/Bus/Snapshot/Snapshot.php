<?php

namespace App\Infrastructure\Shared\Bus\Snapshot;

use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;

final class Snapshot
{
    private string $aggregateClass;
    private string $aggregateId;
    private AggregateRootInterface $aggregateRoot;
    private int $version;
    private \DateTimeImmutable $createdDate;

    public function __construct(
        string $aggregateClass,
        string $aggregateId,
        AggregateRootInterface $aggregateRoot,
        int $version,
        \DateTimeImmutable $createdDate)
    {
        $this->aggregateClass = $aggregateClass;
        $this->aggregateId = $aggregateId;
        $this->aggregateRoot = $aggregateRoot;
        $this->version = $version;
        $this->createdDate = $createdDate;
    }

    public function getAggregateClass(): string
    {
        return $this->aggregateClass;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getAggregateRoot(): AggregateRootInterface
    {
        return $this->aggregateRoot;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getCreatedDate(): \DateTimeImmutable
    {
        return $this->createdDate;
    }
}