<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Persist;

use App\Infrastructure\Shared\Bus\Snapshot\Entity\SnapshotEntity;
use App\Infrastructure\Shared\Bus\Snapshot\Exception\SnapshotNotFoundException;
use App\Infrastructure\Shared\Bus\Snapshot\Factory\SnapshotFactory;
use App\Infrastructure\Shared\Bus\Snapshot\Snapshot;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Infrastructure\Shared\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineSnapshotStorageRepository implements SnapshotStorageRepositoryInterface
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $jsonSerializer;
    private IdentifyGeneratorInterface $identifyGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        SerializerInterface $jsonSerializer,
        IdentifyGeneratorInterface $identifyGenerator
    )
    {
        $this->entityManager = $entityManager;
        $this->jsonSerializer = $jsonSerializer;
        $this->identifyGenerator = $identifyGenerator;
    }

    public function save(Snapshot $snapshot): void
    {
        $entity = $this->entityManager->find(SnapshotEntity::class, $snapshot->getAggregateId());

        if ($entity === null) {
            $entity = new SnapshotEntity();
            $entity->setId($this->identifyGenerator->generate());
            $entity->setAggregateId($snapshot->getAggregateId());
            $entity->setAggregateClass($snapshot->getAggregateClass());
        }

        $entity->setAggregateObject($this->jsonSerializer->serialize($snapshot->getAggregateRoot()));

        $entity->setLastVersion($snapshot->getVersion());
        $entity->setCreatedDate($snapshot->getCreatedDate());

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function get(string $aggregateId): Snapshot
    {
         $entity = $this->entityManager->find(SnapshotEntity::class, $aggregateId);

         if ($entity === null) {
             throw new SnapshotNotFoundException(
                 sprintf('Snapshot with aggregate id %s not found', $aggregateId)
             );
         }

         $aggregateRoot = $this->jsonSerializer->deserialize(
             $entity->getAggregateObject(),
             $entity->getAggregateClass()
         );

         return SnapshotFactory::createFromAggregate($aggregateRoot);
    }

}