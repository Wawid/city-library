<?php

namespace App\Infrastructure\Shared\Bus\Snapshot\Persist;

use App\Infrastructure\Shared\Bus\Snapshot\Snapshot;

interface SnapshotStorageRepositoryInterface
{
    public function save(Snapshot $snapshot): void;

    public function get(string $aggregateId): Snapshot;
}