<?php

namespace App\Infrastructure\Shared\Bus\Message;

interface MessageBusInterface
{
    public function dispatch(object $object): void;
}