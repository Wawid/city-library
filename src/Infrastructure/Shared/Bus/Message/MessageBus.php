<?php

namespace App\Infrastructure\Shared\Bus\Message;

use Symfony\Component\Messenger\MessageBusInterface as SymfonyMessageBusInterface;

class MessageBus implements MessageBusInterface
{
    private SymfonyMessageBusInterface $symfonyMessageBus;

    public function __construct(SymfonyMessageBusInterface $symfonyMessageBus)
    {
        $this->symfonyMessageBus = $symfonyMessageBus;
    }

    public function dispatch(object $object): void
    {
        $this->symfonyMessageBus->dispatch($object);
    }
}