<?php

namespace App\Infrastructure\Shared\Bus\Event\Persist\Doctrine;

use App\Infrastructure\Shared\Bus\Event\Entity\EventStorageEntity;
use App\Infrastructure\Shared\Bus\Event\Persist\EventStorageRepositoryInterface;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Infrastructure\Shared\Serializer\SerializerInterface;
use App\Infrastructure\Event\DomainMessage;
use App\Infrastructure\Event\DomainMessagesStream;
use Doctrine\ORM\EntityManagerInterface;

class EventStorageRepository implements EventStorageRepositoryInterface
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $jsonSerializer;
    private IdentifyGeneratorInterface $identifyGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        SerializerInterface $jsonSerializer,
        IdentifyGeneratorInterface $identifyGenerator
    )
    {
        $this->entityManager = $entityManager;
        $this->jsonSerializer = $jsonSerializer;
        $this->identifyGenerator = $identifyGenerator;
    }

    public function publish(DomainMessagesStream $domainEvents): void
    {
        /** @var DomainMessage $domainEvent */
        foreach ($domainEvents as $domainEvent) {
            $eventStorageEntity = new EventStorageEntity(
                $this->identifyGenerator->generate(),
                $domainEvent->getId(),
                $domainEvent->getType(),
                $domainEvent->getVersion(),
                $this->jsonSerializer->serialize($domainEvent->getPayload()),
                $domainEvent->getRecordedOn()
            );

            $this->entityManager->persist($eventStorageEntity);
            $this->entityManager->flush();
        }
    }

    public function load(string $aggregateId, int $version): DomainMessagesStream
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('e')
            ->from(EventStorageEntity::class, 'e')
            ->where('e.aggregateId = :aggregateId')
            ->andWhere('e.version > :version')
            ->orderBy('e.version', 'ASC')
            ->setParameter('aggregateId', $aggregateId)
            ->setParameter('version', $version);

        $events = $query->getQuery()->toIterable();

        $domainEvents = [];
        /** @var EventStorageEntity $event */
        foreach($events as $event) {
            $domainEvents[] = new DomainMessage(
                $event->getAggregateId(),
                $event->getVersion(),
                $this->jsonSerializer->deserialize(
                    $event->getBody(),
                    $event->getEventClass(),
                ),
                $event->getOccurredOn()
            );
        }

        return new DomainMessagesStream($domainEvents);
    }
}