<?php

namespace App\Infrastructure\Shared\Bus\Event\Persist\Doctrine;

use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use App\Infrastructure\Shared\Bus\Event\Persist\EventStorageRepositoryInterface;
use App\Infrastructure\Shared\Bus\Snapshot\Exception\SnapshotNotFoundException;
use App\Infrastructure\Shared\Bus\Snapshot\Factory\SnapshotFactory;
use App\Infrastructure\Shared\Bus\Snapshot\Persist\SnapshotStorageRepositoryInterface;
use App\Infrastructure\Shared\Bus\Snapshot\Trigger\Trigger;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;
use App\Infrastructure\Shared\Exception\AggregateDoesNotExistException;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;
use App\Infrastructure\Event\Dispatcher\DispatcherInterface;

class AggregateRootRepository implements AggregateRootRepositoryInterface
{
    private EventStorageRepositoryInterface $eventStorageRepository;
    private Trigger $snapshotTrigger;
    private SnapshotStorageRepositoryInterface $snapshotStorageRepository;
    private DispatcherInterface $eventDispatcher;

    public function __construct(
        EventStorageRepositoryInterface $eventStorageRepository,
        Trigger $snapshotTrigger,
        SnapshotStorageRepositoryInterface $snapshotStorageRepository,
        DispatcherInterface $eventDispatcher
    ) {
        $this->eventStorageRepository = $eventStorageRepository;
        $this->snapshotTrigger = $snapshotTrigger;
        $this->snapshotStorageRepository = $snapshotStorageRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function save(AggregateRootInterface $aggregateRoot): void
    {
        $shouldCreateSnapshot = $this->snapshotTrigger->shouldCreateSnapshot($aggregateRoot);

        $events = $aggregateRoot->pullDomainEvents();
        $this->eventStorageRepository->publish($events);

        $this->eventDispatcher->dispatch($events);

        if ($shouldCreateSnapshot) {
            $this->snapshotStorageRepository->save(SnapshotFactory::createFromAggregate($aggregateRoot));
        }
    }

    public function find(IdentifyInterface $aggregateId, string $aggregateClass): AggregateRootInterface
    {
        if ($this->eventStorageRepository->load($aggregateId->getValue(), 0)->getIterator()->count() < 1) {
            throw new AggregateDoesNotExistException(sprintf(
                'Aggregate with id %s does not exist',
                $aggregateId->getValue()
            ));
        }

        try {
            $snapshot = $this->snapshotStorageRepository->get(
                $aggregateId->getValue()
            );

            $aggregateRoot = $snapshot->getAggregateRoot();
        } catch (SnapshotNotFoundException $e) {

            $reflection = new \ReflectionClass($aggregateClass);
            $aggregateRoot = $reflection->newInstanceWithoutConstructor();
        }

        $events = $this->eventStorageRepository->load($aggregateId->getValue(), $aggregateRoot->getVersion());

        if ($events->getIterator()->count() > 0) {
            $aggregateRoot->replay($events);
        }

        return $aggregateRoot;
    }
}