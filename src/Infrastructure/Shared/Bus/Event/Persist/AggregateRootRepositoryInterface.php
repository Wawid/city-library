<?php

namespace App\Infrastructure\Shared\Bus\Event\Persist;

use App\Infrastructure\Shared\Domain\Aggregate\AggregateRootInterface;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;

interface AggregateRootRepositoryInterface
{
    public function save(AggregateRootInterface $aggregateRoot): void;

    public function find(IdentifyInterface $aggregateId, string $aggregateClass): AggregateRootInterface;
}