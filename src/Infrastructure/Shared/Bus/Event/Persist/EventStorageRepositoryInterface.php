<?php

namespace App\Infrastructure\Shared\Bus\Event\Persist;

use App\Infrastructure\Event\DomainMessagesStream;

interface EventStorageRepositoryInterface
{
    public function publish(DomainMessagesStream $domainEvent): void;

    public function load(string $aggregateId, int $version): DomainMessagesStream;
}