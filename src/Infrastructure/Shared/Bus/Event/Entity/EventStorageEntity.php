<?php

namespace App\Infrastructure\Shared\Bus\Event\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="src/Infrastructure/Shared/Bus/Event/Persist/Doctrine/EventStorageRepository")
 */
class EventStorageEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $aggregateId;

    /**
     * @ORM\Column(type="string")
     */
    private string $eventClass;

    /**
     * @ORM\Column(type="integer")
     */
    private int $version;

    /**
     * @ORM\Column(type="string")
     */
    private string $body;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $occurredOn;

    public function __construct(
        string $id,
        string $aggregateId,
        string $eventClass,
        int $version,
        string $body,
        DateTimeImmutable $occurredOn
    )
    {
        $this->id = $id;
        $this->aggregateId = $aggregateId;
        $this->eventClass = $eventClass;
        $this->version = $version;
        $this->body = $body;
        $this->occurredOn = $occurredOn;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getEventClass(): string
    {
        return $this->eventClass;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getOccurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}