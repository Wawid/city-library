<?php

namespace App\Infrastructure\Shared\Bus\Event;

interface DomainEventInterface
{
    public function getAggregateId(): string;
}