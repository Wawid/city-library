<?php

namespace App\Infrastructure\Shared\Exception;

class AggregateDoesNotExistException extends \Exception
{
}