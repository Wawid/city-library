<?php

namespace App\Infrastructure\Shared\Generator;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid as RamseyUuid;

class UuidRamseyGenerator implements IdentifyGeneratorInterface
{
    public function generate(): string
    {
        return RamseyUuid::uuid4()->toString();
    }
}