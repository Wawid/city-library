<?php

namespace App\Infrastructure\Shared\Generator;

interface IdentifyGeneratorInterface
{
    public function generate(): string;
}