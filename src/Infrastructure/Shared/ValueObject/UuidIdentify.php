<?php

namespace App\Infrastructure\Shared\ValueObject;

use Assert\Assertion;

class UuidIdentify implements IdentifyInterface
{
    private string $value;

    public function __construct(string $value)
    {
        Assertion::uuid($value);
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

}