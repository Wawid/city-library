<?php

namespace App\Infrastructure\Shared\ValueObject;

interface IdentifyInterface
{
    public function getValue(): string;
}