<?php

namespace App\Infrastructure\Shared\Domain\Aggregate;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;
use App\Infrastructure\Event\DomainMessagesStream;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;

interface AggregateRootInterface
{
    public function pullDomainEvents(): DomainMessagesStream;

    public function record(DomainEventInterface $domainEvent): void;

    public function replay(DomainMessagesStream $domainMessagesStream): void;

    public function getVersion(): int;

    public function getAggregateId(): IdentifyInterface;
}