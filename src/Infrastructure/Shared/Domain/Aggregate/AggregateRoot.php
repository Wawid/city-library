<?php

namespace App\Infrastructure\Shared\Domain\Aggregate;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;
use App\Infrastructure\Event\DomainMessage;
use App\Infrastructure\Event\DomainMessagesStream;

abstract class AggregateRoot implements AggregateRootInterface
{
    /** @var DomainMessage[] */
    private array $domainEvents = [];
    private int $version = 0;

    final public function pullDomainEvents(): DomainMessagesStream
    {
        $domainEvents = new DomainMessagesStream($this->domainEvents);
        $this->domainEvents = [];

        return $domainEvents;
    }

    final public function record(DomainEventInterface $domainEvent): void
    {
        ++$this->version;

        $this->apply($domainEvent);

        $event = DomainMessage::recordNow(
            $domainEvent->getAggregateId(),
            $this->version,
            $domainEvent
        );

        $this->domainEvents[] = $event;
    }

    private function apply(DomainEventInterface $event): void
    {
        $method = $this->getApplyMethod($event);

        if (!method_exists($this, $method)) {
            return;
        }

        $this->$method($event);
    }

    private function getApplyMethod(DomainEventInterface $event): string
    {
        $classParts = explode('\\', get_class($event));

        return 'apply'.end($classParts);
    }

    public function replay(DomainMessagesStream $domainMessagesStream): void
    {
        /** @var DomainMessage $domainMessage */
        foreach ($domainMessagesStream as $domainMessage) {
            /** @var DomainEventInterface $event */
            $event = $domainMessage->getPayload();
            $this->apply($event);
        }
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function clearRecordedEvents(): void
    {
        $this->domainEvents = [];
    }
}