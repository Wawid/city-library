<?php

namespace App\Infrastructure\Event;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * @phpstan-implements IteratorAggregate<int, DomainMessage>
 */
class DomainMessagesStream implements IteratorAggregate
{
    /**
     * @var DomainMessage[]
     */
    private array $events;

    /**
     * @param DomainMessage[] $events
     */
    public function __construct(array $events)
    {
        $this->events = $events;
    }

    /**
     * @return ArrayIterator<int, DomainMessage>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->events);
    }
}