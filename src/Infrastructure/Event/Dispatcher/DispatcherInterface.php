<?php

namespace App\Infrastructure\Event\Dispatcher;

use App\Infrastructure\Event\DomainMessagesStream;

interface DispatcherInterface
{
    public function dispatch(DomainMessagesStream $events): void;
}