<?php

namespace App\Infrastructure\Event\Dispatcher;

use App\Infrastructure\Event\DomainMessage;
use App\Infrastructure\Event\DomainMessagesStream;
use App\Infrastructure\Shared\Bus\Message\MessageBusInterface;

class EventDispatcher implements DispatcherInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function dispatch(DomainMessagesStream $events): void
    {
        /** @var DomainMessage $event */
        foreach ($events as $event) {
            $this->messageBus->dispatch($event->getPayload());
        }
    }
}