<?php

namespace App\Infrastructure\Event;

use DateTimeImmutable;

final class DomainMessage
{
    private string $id;
    private int $version;
    private object $payload;
    private DateTimeImmutable $recordedOn;

    public function __construct(string $id, int $version, object $payload, \DateTimeImmutable $recordedOn)
    {
        $this->id = $id;
        $this->version = $version;
        $this->payload = $payload;
        $this->recordedOn = $recordedOn;
    }

    public static function recordNow(string $id, int $version, object $payload): self
    {
        return new self(
            $id,
            $version,
            $payload,
            new \DateTimeImmutable('now', new \DateTimeZone('Europe/Warsaw'))
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getPayload(): object
    {
        return $this->payload;
    }

    public function getRecordedOn(): \DateTimeImmutable
    {
        return $this->recordedOn;
    }

    public function getType(): string
    {
        return get_class($this->payload);
    }
}