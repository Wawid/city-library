<?php

namespace App\Domain\Reader\Query;

class FindReader
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}