<?php

namespace App\Domain\Reader\QueryHandler;

use App\Domain\Reader\Query\FindReader;
use App\Domain\Reader\ReadModel\Entity\Reader;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FindReaderHandler implements MessageHandlerInterface
{
    private ReaderRepositoryInterface $readerRepository;

    public function __construct(ReaderRepositoryInterface $readerRepository)
    {
        $this->readerRepository = $readerRepository;
    }

    public function __invoke(FindReader $query): ?Reader
    {
        return $this->readerRepository->find($query->getId());
    }
}