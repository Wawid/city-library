<?php

namespace App\Domain\Reader\Command;

class ChangeReaderName
{
    private string $id;
    private string $name;

    public function __construct(string $id, string $name)
    {
        $this->name = $name;
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}