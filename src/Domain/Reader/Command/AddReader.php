<?php

namespace App\Domain\Reader\Command;

class AddReader
{
    private string $id;
    private string $email;
    private string $name;
    private string $lastName;

    public function __construct(string $id, string $email, string $name, string $lastName)
    {
        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
        $this->lastName = $lastName;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}