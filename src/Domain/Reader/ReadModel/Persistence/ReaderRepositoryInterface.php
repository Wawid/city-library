<?php

namespace App\Domain\Reader\ReadModel\Persistence;

use App\Domain\Reader\ReadModel\Entity\Reader;

interface ReaderRepositoryInterface
{
    public function save(Reader $reader): void;

    public function remove(Reader $reader): void;

    public function find(string $id): ?Reader;
}