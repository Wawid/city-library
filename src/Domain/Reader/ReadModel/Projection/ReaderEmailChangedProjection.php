<?php

namespace App\Domain\Reader\ReadModel\Projection;

use App\Domain\Reader\Event\ReaderEmailChanged;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ReaderEmailChangedProjection implements MessageHandlerInterface
{
    private ReaderRepositoryInterface $readerRepository;

    public function __construct(ReaderRepositoryInterface $readerRepository)
    {
        $this->readerRepository = $readerRepository;
    }

    public function __invoke(ReaderEmailChanged $event): void
    {
        $reader = $this->readerRepository->find($event->getAggregateId());
        $reader->setEmail($event->getEmail());

        $this->readerRepository->save($reader);
    }
}