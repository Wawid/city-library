<?php

namespace App\Domain\Reader\ReadModel\Projection;

use App\Domain\Reader\Event\ReaderAdded;
use App\Domain\Reader\ReadModel\Entity\Reader;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ReaderAddedProjection implements MessageHandlerInterface
{
    private ReaderRepositoryInterface $readerRepository;

    public function __construct(ReaderRepositoryInterface $readerRepository)
    {
        $this->readerRepository = $readerRepository;
    }

    public function __invoke(ReaderAdded $event): void
    {
        $reader = new Reader(
            $event->getAggregateId(),
            $event->getEmail(),
            $event->getName(),
            $event->getLastName()
        );

        $this->readerRepository->save($reader);
    }
}