<?php

namespace App\Domain\Reader\ReadModel\Projection;

use App\Domain\Reader\Event\ReaderNameChanged;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ReaderNameChangeProjection implements MessageHandlerInterface
{
    private ReaderRepositoryInterface $readerRepository;

    public function __construct(ReaderRepositoryInterface $readerRepository)
    {
        $this->readerRepository = $readerRepository;
    }

    public function __invoke(ReaderNameChanged $event): void
    {
        $reader = $this->readerRepository->find($event->getAggregateId());
        $reader->setName($event->getName());

        $this->readerRepository->save($reader);
    }
}