<?php

namespace App\Domain\Reader\Service;

use App\Domain\Reader\Exception\ReaderDoesNotExistsException;
use App\Domain\Reader\Query\FindReader;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class ReaderAsserter
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function assertReaderExist(string $readerId): void
    {
        $reader = $this->handle(new FindReader($readerId));

        if ($reader === null) {
            throw new ReaderDoesNotExistsException(sprintf(
                'Reader with id %s does not exist',
                $readerId
            ));
        }
    }
}