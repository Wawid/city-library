<?php

namespace App\Domain\Reader\CommandHandler;

use App\Domain\Reader\Command\ChangeReaderName;
use App\Domain\Reader\Entity\Reader;
use App\Domain\Reader\ValueObject\Id;
use App\Domain\Reader\ValueObject\Name;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeReaderNameHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRootRepository)
    {
        $this->aggregateRootRepository = $aggregateRootRepository;
    }

    public function __invoke(ChangeReaderName $command): void
    {
        /** @var Reader $reader */
        $reader = $this->aggregateRootRepository->find(new Id($command->getId()), Reader::class);
        $reader->changeName(new Name($command->getName()));

        $this->aggregateRootRepository->save($reader);
    }
}