<?php

namespace App\Domain\Reader\CommandHandler;

use App\Domain\Reader\Command\AddReader;
use App\Domain\Reader\Entity\Reader;
use App\Domain\Reader\ValueObject\Email;
use App\Domain\Reader\ValueObject\Id;
use App\Domain\Reader\ValueObject\LastName;
use App\Domain\Reader\ValueObject\Name;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddReaderHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRootRepository)
    {
        $this->aggregateRootRepository = $aggregateRootRepository;
    }

    public function __invoke(AddReader $command): void
    {
        $reader = new Reader(
            new Id($command->getId()),
            new Email($command->getEmail()),
            new Name($command->getName()),
            new LastName($command->getLastName())
        );

        $this->aggregateRootRepository->save($reader);
    }
}