<?php

namespace App\Domain\Reader\CommandHandler;

use App\Domain\Reader\Command\ChangeReaderEmail;
use App\Domain\Reader\Entity\Reader;
use App\Domain\Reader\ValueObject\Email;
use App\Domain\Reader\ValueObject\Id;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeReaderEmailHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRootRepository)
    {
        $this->aggregateRootRepository = $aggregateRootRepository;
    }

    public function __invoke(ChangeReaderEmail $command): void
    {
        /** @var Reader $reader */
        $reader = $this->aggregateRootRepository->find(new Id($command->getId()), Reader::class);
        $reader->changeEmail(new Email($command->getEmail()));

        $this->aggregateRootRepository->save($reader);
    }
}