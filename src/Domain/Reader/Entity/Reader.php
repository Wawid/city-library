<?php

namespace App\Domain\Reader\Entity;

use App\Domain\Reader\Event\ReaderAdded;
use App\Domain\Reader\Event\ReaderEmailChanged;
use App\Domain\Reader\Event\ReaderNameChanged;
use App\Domain\Reader\ValueObject\Email;
use App\Domain\Reader\ValueObject\Id;
use App\Domain\Reader\ValueObject\LastName;
use App\Domain\Reader\ValueObject\Name;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRoot;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;

class Reader extends AggregateRoot
{
    private Id $aggregateId;
    private Email $email;
    private Name $name;
    private LastName $lastName;

    public function __construct(Id $aggregateId, Email $email, Name $name, LastName $lastName)
    {
        $this->record(new ReaderAdded(
            $aggregateId->getValue(),
            $email->getEmail(),
            $name->getName(),
            $lastName->getLastName()
        ));
    }

    protected function applyReaderAdded(ReaderAdded $event): void
    {
        $this->aggregateId = new Id($event->getAggregateId());
        $this->email = new Email($event->getEmail());
        $this->name = new Name($event->getName());
        $this->lastName = new LastName($event->getLastName());
    }

    public function changeName(Name $name): void
    {
        $this->record(new ReaderNameChanged(
            $this->aggregateId->getValue(),
            $name->getName()
        ));
    }

    public function applyReaderNameChanged(ReaderNameChanged $event): void
    {
        $this->name = new Name($event->getName());
    }

    public function changeEmail(Email $email): void
    {
        $this->record(new ReaderEmailChanged(
            $this->aggregateId->getValue(),
            $email->getEmail()
        ));
    }

    public function applyReaderEmailChanged(ReaderEmailChanged $event): void
    {
        $this->email = new Email($event->getEmail());
    }

    public function getAggregateId(): IdentifyInterface
    {
        return $this->aggregateId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }
}