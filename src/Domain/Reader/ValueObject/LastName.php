<?php

namespace App\Domain\Reader\ValueObject;

use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;

final class LastName
{
    private string $lastName;

    public function __construct(string $lastName)
    {
        $char = trim($lastName);

        if (mb_strlen($char) < 1) {
            throw new MinValueException(
                sprintf('Value should has minimum 1 char. Receive %s', $char)
            );
        }

        if (mb_strlen($char) > 255) {
            throw new MaxValueException(
                sprintf('Value should has maximum 255 char. Receive %s', $char)
            );
        }

        $this->lastName = $lastName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}