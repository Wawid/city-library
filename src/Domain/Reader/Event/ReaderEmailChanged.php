<?php

namespace App\Domain\Reader\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class ReaderEmailChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $email;

    public function __construct(string $aggregateId, string $email)
    {
        $this->aggregateId = $aggregateId;
        $this->email = $email;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}