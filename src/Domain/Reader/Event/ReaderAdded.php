<?php

namespace App\Domain\Reader\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class ReaderAdded implements DomainEventInterface
{
    private string $aggregateId;
    private string $email;
    private string $name;
    private string $lastName;

    public function __construct(string $aggregateId, string $email, string $name, string $lastName)
    {
        $this->aggregateId = $aggregateId;
        $this->email = $email;
        $this->name = $name;
        $this->lastName = $lastName;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}