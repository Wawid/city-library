<?php

namespace App\Domain\Reader\Exception;

class EmailMustNotBeEmptyException extends \LogicException
{
}