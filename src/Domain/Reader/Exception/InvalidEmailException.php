<?php

namespace App\Domain\Reader\Exception;

class InvalidEmailException extends \LogicException
{
}