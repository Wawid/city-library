<?php

namespace App\Domain\Reader\Exception;

class ReaderDoesNotExistsException extends \LogicException
{
}