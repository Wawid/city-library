<?php

namespace App\Domain\User\CommandHandler;

use App\Domain\User\Command\CreateUser;
use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use App\Domain\User\ValueObject\Id;
use App\Domain\User\ValueObject\Password;
use App\Domain\User\ValueObject\Role;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateUserHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRootRepository)
    {
        $this->aggregateRootRepository = $aggregateRootRepository;
    }

    public function __invoke(CreateUser $command): void
    {
        $user = new User(
            new Id($command->getId()),
            new Email($command->getEmail()),
            new Password($command->getPassword()),
            new Role($command->getRoles())
        );

        $this->aggregateRootRepository->save($user);
    }
}