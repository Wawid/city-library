<?php

namespace App\Domain\User\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class UserCreated implements DomainEventInterface
{
    private string $id;
    private string $email;
    private string $password;
    /** @var string[] */
    private array $roles;

    /** @param string[] $roles */
    public function __construct(string $id, string $email, string $password, array $roles)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
    }

    public function getAggregateId(): string
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}