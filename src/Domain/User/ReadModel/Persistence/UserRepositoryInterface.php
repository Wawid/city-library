<?php

namespace App\Domain\User\ReadModel\Persistence;

use App\Domain\User\ReadModel\Entity\User;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function remove(User $user): void;

    public function find(string $id): ?User;
}