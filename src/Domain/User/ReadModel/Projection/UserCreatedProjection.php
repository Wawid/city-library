<?php

namespace App\Domain\User\ReadModel\Projection;

use App\Domain\User\Event\UserCreated;
use App\Domain\User\ReadModel\Factory\UserFactory;
use App\Domain\User\ReadModel\Persistence\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserCreatedProjection implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private UserFactory $userFactory;

    public function __construct(UserRepositoryInterface $userRepository, UserFactory $userFactory)
    {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }

    public function __invoke(UserCreated $event): void
    {
        $user = $this->userFactory->createUser(
            $event->getAggregateId(),
            $event->getEmail(),
            $event->getPassword(),
            $event->getRoles()
        );

        $this->userRepository->save($user);
    }
}