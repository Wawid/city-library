<?php

namespace App\Domain\User\ReadModel\Factory;

use App\Domain\User\ReadModel\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFactory
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @param string[] $roles
     */
    public function createUser(
        string $id,
        string $email,
        string $password,
        array $roles
    ): User
    {
        $user =  new User($id, $email, $roles);

        $password = $this->userPasswordHasher->hashPassword($user, $password);
        $user->setPassword($password);

        return $user;
    }
}