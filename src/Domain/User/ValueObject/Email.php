<?php

namespace App\Domain\User\ValueObject;

use App\Domain\Reader\Exception\EmailMustNotBeEmptyException;
use App\Domain\Reader\Exception\InvalidEmailException;

class Email
{
    private string $email;

    public function __construct(string $email)
    {
        if (empty($email)) {
            throw new EmailMustNotBeEmptyException('Email must not be empty');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException('Email address is incorrect');
        }

        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}