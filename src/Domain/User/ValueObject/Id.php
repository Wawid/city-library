<?php

namespace App\Domain\User\ValueObject;

use App\Infrastructure\Shared\ValueObject\UuidIdentify;

final class Id extends UuidIdentify
{
}