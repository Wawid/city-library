<?php

namespace App\Domain\User\Entity;

use App\Domain\User\Event\UserCreated;
use App\Domain\User\ValueObject\Email;
use App\Domain\User\ValueObject\Id;
use App\Domain\User\ValueObject\Password;
use App\Domain\User\ValueObject\Role;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRoot;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;

class User extends AggregateRoot
{
    private Id $aggregateId;
    private Email $email;
    private Password $password;
    private Role $role;

    public function __construct(Id $aggregateId, Email $email, Password $password, Role $role)
    {
        $this->record(new UserCreated(
            $aggregateId->getValue(),
            $email->getEmail(),
            $password->getPassword(),
            $role->getRoles())
        );
    }

    public function applyUserCreated(UserCreated $event): void
    {
        $this->aggregateId = new Id($event->getAggregateId());
        $this->email = new Email($event->getEmail());
        $this->password = new Password($event->getPassword());
        $this->role = new Role($event->getRoles());
    }

    public function getAggregateId(): IdentifyInterface
    {
        return $this->aggregateId;
    }

}