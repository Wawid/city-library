<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookCoverChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $bookCover;

    public function __construct(string $aggregateId, string $bookCover)
    {
        $this->aggregateId = $aggregateId;
        $this->bookCover = $bookCover;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getBookCover(): string
    {
        return $this->bookCover;
    }
}