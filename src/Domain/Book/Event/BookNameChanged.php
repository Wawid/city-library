<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookNameChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $name;

    public function __construct(string $aggregateId, string $name)
    {
        $this->aggregateId = $aggregateId;
        $this->name = $name;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}