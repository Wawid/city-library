<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookPublicationDateChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $publicationDate;

    public function __construct(string $aggregateId, string $publicationDate)
    {
        $this->aggregateId = $aggregateId;
        $this->publicationDate = $publicationDate;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getPublicationDate(): string
    {
        return $this->publicationDate;
    }
}