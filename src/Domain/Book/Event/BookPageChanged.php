<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookPageChanged implements DomainEventInterface
{
    private string $aggregateId;
    private int $page;

    public function __construct(string $aggregateId, int $page)
    {
        $this->aggregateId = $aggregateId;
        $this->page = $page;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getPage(): int
    {
        return $this->page;
    }


}