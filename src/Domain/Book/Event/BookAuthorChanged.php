<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookAuthorChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $author;

    public function __construct(string $aggregateId, string $author)
    {
        $this->aggregateId = $aggregateId;
        $this->author = $author;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}