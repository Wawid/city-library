<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookIsbnChanged implements DomainEventInterface
{
    private string $aggregateId;
    private string $isbn;

    public function __construct(string $aggregateId, string $isbn)
    {
        $this->aggregateId = $aggregateId;
        $this->isbn = $isbn;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }
}