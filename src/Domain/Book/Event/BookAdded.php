<?php

namespace App\Domain\Book\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookAdded implements DomainEventInterface
{
    private string $aggregateId;
    private string $name;
    private string $author;
    private string $bookCover;
    private string $isbn;
    private int $page;
    private string $publicationDate;

    public function __construct(
        string $aggregateId,
        string $name,
        string $author,
        string $bookCover,
        string $isbn,
        int $page,
        string $publicationDate,

    )
    {
        $this->aggregateId = $aggregateId;
        $this->name = $name;
        $this->author = $author;
        $this->bookCover = $bookCover;
        $this->isbn = $isbn;
        $this->page = $page;
        $this->publicationDate = $publicationDate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAggregateId(): string
    {
       return $this->aggregateId;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getBookCover(): string
    {
        return $this->bookCover;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPublicationDate(): string
    {
        return $this->publicationDate;
    }
}