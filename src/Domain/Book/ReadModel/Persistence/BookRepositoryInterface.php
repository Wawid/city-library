<?php

namespace App\Domain\Book\ReadModel\Persistence;

use App\Domain\Book\ReadModel\Entity\Book;

interface BookRepositoryInterface
{
    public function save(Book $book): void;

    public function remove(Book $book): void;

    public function find(string $id): ?Book;
}