<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookIsbnChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookIsbnChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookIsbnChanged $event): void
    {
        $entity = $this->bookRepository->find($event->getAggregateId());
        $entity->setIsbn($event->getIsbn());

        $this->bookRepository->save($entity);
    }
}