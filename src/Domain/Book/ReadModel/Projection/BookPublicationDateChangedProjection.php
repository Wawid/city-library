<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookPublicationDateChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookPublicationDateChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookPublicationDateChanged $event): void
    {
        $book = $this->bookRepository->find($event->getAggregateId());
        $book->setPublicationDate(new \DateTimeImmutable($event->getPublicationDate()));

        $this->bookRepository->save($book);
    }
}