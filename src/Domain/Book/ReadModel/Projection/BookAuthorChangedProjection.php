<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookAuthorChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookAuthorChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookAuthorChanged $event): void
    {
        $entity = $this->bookRepository->find($event->getAggregateId());
        $entity->setAuthor($event->getAuthor());

        $this->bookRepository->save($entity);
    }
}