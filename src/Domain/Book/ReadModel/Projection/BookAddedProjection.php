<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookAdded;
use App\Domain\Book\ReadModel\Entity\Book;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookAddedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookAdded $event): void
    {
        $book = new Book(
            $event->getAggregateId(),
            $event->getName(),
            $event->getAuthor(),
            $event->getBookCover(),
            $event->getIsbn(),
            $event->getPage(),
            new \DateTimeImmutable($event->getPublicationDate())
        );

        $this->bookRepository->save($book);
    }
}