<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookNameChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookNameChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookNameChanged $event): void
    {
        $entity = $this->bookRepository->find($event->getAggregateId());
        $entity->setName($event->getName());

        $this->bookRepository->save($entity);
    }
}