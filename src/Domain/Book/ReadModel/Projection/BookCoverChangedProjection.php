<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookCoverChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookCoverChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookCoverChanged $event): void
    {
        $book = $this->bookRepository->find($event->getAggregateId());
        $book->setBookCover($event->getBookCover());

        $this->bookRepository->save($book);
    }
}