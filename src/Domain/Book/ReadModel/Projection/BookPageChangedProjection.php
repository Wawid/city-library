<?php

namespace App\Domain\Book\ReadModel\Projection;

use App\Domain\Book\Event\BookPageChanged;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookPageChangedProjection implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(BookPageChanged $event): void
    {
        $book = $this->bookRepository->find($event->getAggregateId());
        $book->setPage($event->getPage());

        $this->bookRepository->save($book);
    }
}