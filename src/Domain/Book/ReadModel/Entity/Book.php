<?php

namespace App\Domain\Book\ReadModel\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="src/Infrastructure/Persistence/Book/Doctrine/BookRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $author;

    /**
     * @ORM\Column(type="string")
     */
    private string $bookCover;

    /**
     * @ORM\Column(type="string")
     */
    private string $isbn;

    /**
     * @ORM\Column(type="integer")
     */
    private int $page;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $publicationDate;

    public function __construct(
        string $id,
        string $name,
        string $author,
        string $bookCover,
        string $isbn,
        int $page,
        \DateTimeImmutable $publicationDate
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->bookCover = $bookCover;
        $this->isbn = $isbn;
        $this->page = $page;
        $this->publicationDate = $publicationDate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getBookCover(): string
    {
        return $this->bookCover;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): void
    {
        $this->isbn = $isbn;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    public function setBookCover(string $bookCover): void
    {
        $this->bookCover = $bookCover;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    public function getPublicationDate(): \DateTimeImmutable
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeImmutable $publicationDate): void
    {
        $this->publicationDate = $publicationDate;
    }
}