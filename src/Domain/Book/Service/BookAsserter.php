<?php

namespace App\Domain\Book\Service;

use App\Domain\Book\Exception\BookDoesNotExistsException;
use App\Domain\Book\Query\FindBook;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class BookAsserter
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function assertBookExist(string $bookId): void
    {
        $book = $this->handle(new FindBook($bookId));

        if ($book === null) {
            throw new BookDoesNotExistsException(sprintf(
                'Book with id %s does not exist',
                $bookId
            ));
        }
    }
}