<?php

namespace App\Domain\Book\Exception;

class IncorrectIsbnException extends \Exception
{
}