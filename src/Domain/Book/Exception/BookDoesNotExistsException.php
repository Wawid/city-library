<?php

namespace App\Domain\Book\Exception;

class BookDoesNotExistsException extends \LogicException
{
}