<?php

namespace App\Domain\Book\Command;

class ChangeBookPublicationDate
{
    private string $id;
    private string $publicationDate;

    public function __construct(string $id, string $publicationDate)
    {
        $this->id = $id;
        $this->publicationDate = $publicationDate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPublicationDate(): string
    {
        return $this->publicationDate;
    }
}