<?php

namespace App\Domain\Book\Command;

class AddBook
{
    private string $id;
    private string $name;
    private string $author;
    private string $bookCover;
    private string $isbn;
    private int $page;
    private string $publicationDate;

    public function __construct(
        string $id,
        string $name,
        string $author,
        string $bookCover,
        string $isbn,
        int $page,
        string $publicationDate
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->bookCover = $bookCover;
        $this->isbn = $isbn;
        $this->page = $page;
        $this->publicationDate = $publicationDate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getBookCover(): string
    {
        return $this->bookCover;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPublicationDate(): string
    {
        return $this->publicationDate;
    }
}