<?php

namespace App\Domain\Book\Command;

class ChangeBookPage
{
    private string $id;
    private int $page;

    public function __construct(int $page, string $id)
    {
        $this->page = $page;
        $this->id = $id;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getId(): string
    {
        return $this->id;
    }
}