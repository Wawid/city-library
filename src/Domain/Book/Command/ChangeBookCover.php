<?php

namespace App\Domain\Book\Command;

class ChangeBookCover
{
    private string $id;
    private string $cover;

    public function __construct(string $id, string $cover)
    {
        $this->id = $id;
        $this->cover = $cover;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCover(): string
    {
        return $this->cover;
    }
}