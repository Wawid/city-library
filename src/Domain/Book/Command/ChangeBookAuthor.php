<?php

namespace App\Domain\Book\Command;

class ChangeBookAuthor
{
    private string $id;
    private string $author;

    public function __construct(string $id, string $author)
    {
        $this->id = $id;
        $this->author = $author;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}