<?php

namespace App\Domain\Book\Command;

class ChangeBookName
{
    private string $id;
    private string $name;

    public function __construct(string $name, string $aggregateId)
    {
        $this->name = $name;
        $this->id = $aggregateId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): string
    {
        return $this->id;
    }
}