<?php

namespace App\Domain\Book\Command;

class ChangeBookIsbn
{
    private string $id;
    private string $isbn;

    public function __construct(string $id, string $isbn)
    {
        $this->id = $id;
        $this->isbn = $isbn;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }
}