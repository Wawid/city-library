<?php

namespace App\Domain\Book\Entity;

use App\Domain\Book\Event\BookAdded;
use App\Domain\Book\Event\BookAuthorChanged;
use App\Domain\Book\Event\BookCoverChanged;
use App\Domain\Book\Event\BookIsbnChanged;
use App\Domain\Book\Event\BookNameChanged;
use App\Domain\Book\Event\BookPageChanged;
use App\Domain\Book\Event\BookPublicationDateChanged;
use App\Domain\Book\Registry\IsbnRegistryInterface;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Isbn;
use App\Domain\Book\ValueObject\Name;
use App\Domain\Book\ValueObject\Page;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRoot;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;

final class Book extends AggregateRoot
{
    private Id $aggregateId;
    private Name $name;
    private Author $author;
    private BookCover $bookCover;
    private Isbn $isbn;
    private Page $page;
    private \DateTimeInterface $publicationDate;

    public function __construct(
        Id $aggregateId,
        Name $name,
        Author $author,
        BookCover $bookCover,
        Isbn $isbn,
        Page $page,
        \DateTimeImmutable $publicationDate,
        IsbnRegistryInterface $isbnRegistry
    ) {
        $isbnRegistry->assertIsbnNotExists($isbn->getIsbn());

        $this->record(new BookAdded(
            $aggregateId->getValue(),
            $name->getName(),
            $author->getAuthor(),
            $bookCover->getValue(),
            $isbn->getIsbn(),
            $page->getPage(),
            $publicationDate->format('Y-m-d')
        ));
    }

    protected function applyBookAdded(BookAdded $event): void
    {
        $this->aggregateId = new Id($event->getAggregateId());
        $this->name = new Name($event->getName());
        $this->author = new Author($event->getAuthor());
        $this->bookCover = new BookCover($event->getBookCover());
        $this->isbn = new Isbn($event->getIsbn());
        $this->page = new Page($event->getPage());
        $this->publicationDate = new \DateTimeImmutable(
            $event->getPublicationDate(),
            new \DateTimeZone('Europe/Warsaw')
        );
    }

    public function changeName(Name $name): void
    {
        $this->record(new BookNameChanged(
                $this->aggregateId->getValue(),
                $name->getName())
        );
    }

    protected function applyBookNameChanged(BookNameChanged $event): void
    {
        $this->name = new Name($event->getName());
    }

    public function changeAuthor(Author $author): void
    {
        $this->record(new BookAuthorChanged(
            $this->aggregateId->getValue(),
            $author->getAuthor()
        ));
    }

    protected function applyBookAuthorChanged(BookAuthorChanged $event): void
    {
        $this->author = new Author($event->getAuthor());
    }

    public function changeBookCover(BookCover $bookCover): void
    {
        $this->record(new BookCoverChanged(
            $this->getAggregateId()->getValue(),
            $bookCover->getValue()
        ));
    }

    protected function applyBookCoverChanged(BookCoverChanged $event): void
    {
        $this->bookCover = new BookCover($event->getBookCover());
    }

    public function changeIsbn(Isbn $isbn): void
    {
        $this->record(new BookIsbnChanged(
            $this->getAggregateId()->getValue(),
            $isbn->getIsbn()
        ));
    }

    protected function applyBookIsbnChanged(BookIsbnChanged $event): void
    {
        $this->isbn = new Isbn($event->getIsbn());
    }

    public function changePage(Page $page): void
    {
        $this->record(new BookPageChanged(
            $this->aggregateId->getValue(),
            $page->getPage()
        ));
    }

    public function applyBookPageChanged(BookPageChanged $event): void
    {
        $this->page = new Page($event->getPage());
    }

    public function changePublicationDate(\DateTimeImmutable $dateTimeImmutable): void
    {
        $this->record(new BookPublicationDateChanged(
            $this->aggregateId->getValue(),
            $dateTimeImmutable->format('Y-m-d')
        ));
    }

    public function applyBookPublicationDateChanged(BookPublicationDateChanged $event): void
    {
        $this->publicationDate = new \DateTimeImmutable($event->getPublicationDate());
    }

    public function getAggregateId(): IdentifyInterface
    {
        return $this->aggregateId;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function getBookCover(): BookCover
    {
        return $this->bookCover;
    }

    public function getIsbn(): Isbn
    {
        return $this->isbn;
    }

    public function getPage(): Page
    {
        return $this->page;
    }

    public function getPublicationDate(): \DateTimeInterface
    {
        return $this->publicationDate;
    }
}