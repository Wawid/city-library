<?php

namespace App\Domain\Book\QueryHandler;

use App\Domain\Book\Query\FindBook;
use App\Domain\Book\ReadModel\Entity\Book;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FindBookHandler implements MessageHandlerInterface
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function __invoke(FindBook $query): ?Book
    {
        return $this->bookRepository->find($query->getId());
    }
}