<?php

namespace App\Domain\Book\ValueObject;

use App\Infrastructure\Shared\ValueObject\UuidIdentify;

final class Id extends UuidIdentify
{

}