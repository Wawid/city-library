<?php

namespace App\Domain\Book\ValueObject;

use App\Domain\Shared\Exception\MinValueException;

class Page
{
    private int $page;

    public function __construct(int $page)
    {
        if ($page < 1) {
            throw new MinValueException(
                sprintf('Value should be minimum 1. Receive %s', $page)
            );
        }

        $this->page = $page;
    }

    public function getPage(): int
    {
        return $this->page;
    }
}