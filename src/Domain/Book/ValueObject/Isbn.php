<?php

namespace App\Domain\Book\ValueObject;

use App\Domain\Book\Exception\IncorrectIsbnException;

final class Isbn
{
    private string $isbn;

    public function __construct(string $isbn)
    {
        if (!$this->validate($isbn)) {
            throw new IncorrectIsbnException('Incorrect isbn provided');
        }

        $this->isbn = $isbn;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }

    private function validate(string $isbn): bool
    {
        if ($isbn === '') {
            return false;
        }

        $canonical = str_replace('-', '', $isbn);

        $isbnLength = strlen($canonical);

        if ($isbnLength === 10) {
            return $this->validateIsbn10($canonical);
        }

        if ($isbnLength === 13) {
            return $this->validateIsbn13($canonical);
        }

        return false;
    }

    private function validateIsbn10(string $isbn): bool
    {
        $checkSum = 0;

        for ($i = 0; $i < 10; ++$i) {
            if (!isset($isbn[$i])) {
                return false;
            }

            if ('X' === $isbn[$i]) {
                $digit = 10;
            } elseif (ctype_digit($isbn[$i])) {
                $digit = (int) $isbn[$i];
            } else {
                return false;
            }

            $checkSum += $digit * (10 - $i);
        }
        return 0 === $checkSum % 11;
    }

    private function  validateIsbn13(string $isbn): bool
    {
        if (!ctype_digit($isbn)) {
            return false;
        }

        $checkSum = 0;

        for ($i = 0; $i < 13; $i += 2) {
            $checkSum += (int) $isbn[$i];
        }

        for ($i = 1; $i < 12; $i += 2) {
            $checkSum += (int) $isbn[$i] * 3;
        }

        return 0 === $checkSum % 10;
    }
}