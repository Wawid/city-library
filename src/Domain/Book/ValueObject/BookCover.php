<?php

namespace App\Domain\Book\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * @method static BookCover HARD()
 * @method static BookCover SOFT()
 * @extends Enum<string>
 */
final class BookCover extends Enum
{
    protected const HARD = 'hard';
    protected const SOFT = 'soft';
}