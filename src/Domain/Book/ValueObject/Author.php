<?php

namespace App\Domain\Book\ValueObject;

use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;

final class Author
{
    private string $author;

    public function __construct(string $author)
    {
        $char = trim($author);

        if (mb_strlen($char) < 1) {
            throw new MinValueException(
                sprintf('Value should has minimum 1 char. Receive %s', $char)
            );
        }

        if (mb_strlen($char) > 255) {
            throw new MaxValueException(
                sprintf('Value should has maximum 255 char. Receive %s', $char)
            );
        }

        $this->author = $author;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}