<?php

namespace App\Domain\Book\ValueObject;

use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;

final class Name
{
    private string $name;

    public function __construct(string $name)
    {
        $char = trim($name);

        if (mb_strlen($char) < 1) {
            throw new MinValueException(
                sprintf('Value should has minimum 1 char. Receive %s', $char)
            );
        }

        if (mb_strlen($char) > 255) {
            throw new MaxValueException(
                sprintf('Value should has maximum 255 char. Receive %s', $char)
            );
        }

        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}