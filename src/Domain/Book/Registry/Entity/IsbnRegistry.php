<?php

namespace App\Domain\Book\Registry\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="src/Infrastructure/Persistence/IsbnRegistry/Doctrine/IsbnRegistryRepository")
 */
class IsbnRegistry
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $isbn;

    public function __construct(string $id, string $isbn)
    {
        $this->id = $id;
        $this->isbn = $isbn;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getIsbn(): string
    {
        return $this->isbn;
    }
}