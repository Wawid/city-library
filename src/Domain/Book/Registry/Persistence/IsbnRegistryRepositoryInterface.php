<?php

namespace App\Domain\Book\Registry\Persistence;

use App\Domain\Book\Registry\Entity\IsbnRegistry;

interface IsbnRegistryRepositoryInterface
{
    public function save(IsbnRegistry $isbnRegistry): void;

    public function findByIsbn(string $isbn): ?IsbnRegistry;
}