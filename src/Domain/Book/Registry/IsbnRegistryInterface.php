<?php

namespace App\Domain\Book\Registry;

interface IsbnRegistryInterface
{
    public function assertIsbnNotExists(string $isbn): void;
}