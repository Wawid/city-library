<?php

namespace App\Domain\Book\Registry;

use App\Domain\Book\Exception\UniqueIsbnRequireException;
use App\Domain\Book\Registry\Persistence\IsbnRegistryRepositoryInterface;

class IsbnRegistry implements IsbnRegistryInterface
{
    private IsbnRegistryRepositoryInterface $isbnRegistryRepository;

    public function __construct(IsbnRegistryRepositoryInterface $isbnRegistryRepository)
    {
        $this->isbnRegistryRepository = $isbnRegistryRepository;
    }

    public function assertIsbnNotExists(string $isbn): void
    {
        $isbnRegistry = $this->isbnRegistryRepository->findByIsbn($isbn);

        if ($isbnRegistry !== null) {
            throw new UniqueIsbnRequireException(sprintf(
                'ISBN with value %s is occupied',
                $isbn
            ));
        }
    }
}