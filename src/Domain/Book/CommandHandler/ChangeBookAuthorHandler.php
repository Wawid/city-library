<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookAuthor;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\Id;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookAuthorHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRepository)
    {
        $this->aggregateRepository = $aggregateRepository;
    }

    public function __invoke(ChangeBookAuthor $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRepository->find(new Id($command->getId()), Book::class);
        $book->changeAuthor(new Author($command->getAuthor()));

        $this->aggregateRepository->save($book);
    }
}