<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\AddBook;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\Registry\Entity\IsbnRegistry;
use App\Domain\Book\Registry\IsbnRegistryInterface;
use App\Domain\Book\Registry\Persistence\IsbnRegistryRepositoryInterface;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Isbn;
use App\Domain\Book\ValueObject\Name;
use App\Domain\Book\ValueObject\Page;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddBookHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRepository;
    private IsbnRegistryInterface $isbnRegistry;
    private IsbnRegistryRepositoryInterface $isbnRegistryRepository;

    public function __construct(
        AggregateRootRepositoryInterface $aggregateRepository,
        IsbnRegistryInterface $isbnRegistry,
        IsbnRegistryRepositoryInterface $isbnRegistryRepository
    )
    {
        $this->aggregateRepository = $aggregateRepository;
        $this->isbnRegistry = $isbnRegistry;
        $this->isbnRegistryRepository = $isbnRegistryRepository;
    }

    public function __invoke(AddBook $command): void
    {
        $book = new Book(
            new Id($command->getId()),
            new Name($command->getName()),
            new Author($command->getAuthor()),
            new BookCover($command->getBookCover()),
            new Isbn($command->getIsbn()),
            new Page($command->getPage()),
            new \DateTimeImmutable($command->getPublicationDate(), new \DateTimeZone('Europe/Warsaw')),
            $this->isbnRegistry
        );

        $isbnRegistry = new IsbnRegistry($command->getId(), $command->getIsbn());

        $this->isbnRegistryRepository->save($isbnRegistry);
        $this->aggregateRepository->save($book);
    }
}