<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookPublicationDate;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\Id;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookPublicationDateHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRepository)
    {
        $this->aggregateRepository = $aggregateRepository;
    }

    public function __invoke(ChangeBookPublicationDate $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRepository->find(new Id($command->getId()), Book::class);
        $book->changePublicationDate(new \DateTimeImmutable(
            $command->getPublicationDate(),
            new \DateTimeZone('Europe/Warsaw'))
        );

        $this->aggregateRepository->save($book);
    }
}