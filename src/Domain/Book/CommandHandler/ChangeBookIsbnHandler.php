<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookIsbn;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Isbn;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookIsbnHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRepository)
    {
        $this->aggregateRepository = $aggregateRepository;
    }

    public function __invoke(ChangeBookIsbn $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRepository->find(new Id($command->getId()), Book::class);
        $book->changeIsbn(new Isbn($command->getIsbn()));

        $this->aggregateRepository->save($book);
    }
}