<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookPage;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Page;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookPageHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRepository)
    {
        $this->aggregateRepository = $aggregateRepository;
    }

    public function __invoke(ChangeBookPage $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRepository->find(new Id($command->getId()), Book::class);
        $book->changePage(new Page($command->getPage()));

        $this->aggregateRepository->save($book);
    }
}