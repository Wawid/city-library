<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookName;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Name;
use App\Infrastructure\Shared\Bus\Event\Persist\Doctrine\AggregateRootRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookNameHandler implements MessageHandlerInterface
{
    private AggregateRootRepository $aggregateRepository;

    public function __construct(AggregateRootRepository $aggregateRepository)
    {
        $this->aggregateRepository = $aggregateRepository;
    }

    public function __invoke(ChangeBookName $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRepository->find(new Id($command->getId()), Book::class);
        $book->changeName(new Name($command->getName()));

        $this->aggregateRepository->save($book);
    }
}