<?php

namespace App\Domain\Book\CommandHandler;

use App\Domain\Book\Command\ChangeBookCover;
use App\Domain\Book\Entity\Book;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChangeBookCoverHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;

    public function __construct(AggregateRootRepositoryInterface $aggregateRootRepository)
    {
        $this->aggregateRootRepository = $aggregateRootRepository;
    }

    public function __invoke(ChangeBookCover $command): void
    {
        /** @var Book $book */
        $book = $this->aggregateRootRepository->find(new Id($command->getId()), $command->getCover());
        $book->changeBookCover(new BookCover($command->getCover()));

        $this->aggregateRootRepository->save($book);
    }
}