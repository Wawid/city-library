<?php

namespace App\Domain\Borrow\ReadModel\Entity;

use App\Domain\Book\ReadModel\Entity\Book;
use App\Domain\Reader\ReadModel\Entity\Reader;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="src/Infrastructure/Persistence/Borrow/Doctrine/BorrowRepository")
 */
class Borrow
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Book\ReadModel\Entity\Book")
     * @ORM\JoinColumn(nullable=false)
     */
    private Book $book;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Reader\ReadModel\Entity\Reader")
     * @ORM\JoinColumn(nullable=false)
     */
    private Reader $reader;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $borrowBookDate;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $returnBookDate;

    public function __construct(
        string $id,
        Book $book,
        Reader $reader,
        DateTimeImmutable $borrowBookDate,
        DateTimeImmutable $returnBookDate
    ) {
        $this->id = $id;
        $this->book = $book;
        $this->reader = $reader;
        $this->borrowBookDate = $borrowBookDate;
        $this->returnBookDate = $returnBookDate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function getReader(): Reader
    {
        return $this->reader;
    }

    public function getBorrowBookDate(): DateTimeImmutable
    {
        return $this->borrowBookDate;
    }

    public function getReturnBookDate(): DateTimeImmutable
    {
        return $this->returnBookDate;
    }
}