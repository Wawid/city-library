<?php

namespace App\Domain\Borrow\ReadModel\Persistence;

use App\Domain\Borrow\ReadModel\Entity\Borrow;

interface BorrowRepositoryInterface
{
    public function save(Borrow $borrow): void;

    public function remove(Borrow $borrow): void;

    public function find(string $id): ?Borrow;
}