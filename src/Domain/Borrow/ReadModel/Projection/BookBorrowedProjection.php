<?php

namespace App\Domain\Borrow\ReadModel\Projection;

use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use App\Domain\Borrow\Event\BookBorrowed;
use App\Domain\Borrow\ReadModel\Entity\Borrow;
use App\Domain\Borrow\ReadModel\Persistence\BorrowRepositoryInterface;
use App\Domain\Reader\ReadModel\Persistence\ReaderRepositoryInterface;
use DateTimeImmutable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookBorrowedProjection implements MessageHandlerInterface
{
    private BorrowRepositoryInterface $borrowRepository;
    private BookRepositoryInterface $bookRepository;
    private ReaderRepositoryInterface $readerRepository;

    public function __construct(
        BorrowRepositoryInterface $borrowRepository,
        BookRepositoryInterface $bookRepository,
        ReaderRepositoryInterface $readerRepository
    ) {
        $this->borrowRepository = $borrowRepository;
        $this->bookRepository = $bookRepository;
        $this->readerRepository = $readerRepository;
    }

    public function __invoke(BookBorrowed $event): void
    {
        $book = $this->bookRepository->find($event->getBookId());
        $reader = $this->readerRepository->find($event->getReaderId());

        $borrow = new Borrow(
            $event->getAggregateId(),
            $book,
            $reader,
            new DateTimeImmutable($event->getBorrowBookDate()),
            new DateTimeImmutable($event->getReturnBookDate())
        );

        $this->borrowRepository->save($borrow);
    }
}