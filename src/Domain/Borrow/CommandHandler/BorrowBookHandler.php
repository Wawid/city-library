<?php

namespace App\Domain\Borrow\CommandHandler;

use App\Domain\Book\Service\BookAsserter;
use App\Domain\Borrow\Command\BorrowBook;
use App\Domain\Borrow\Entity\Borrow;
use App\Domain\Borrow\ValueObject\BookId;
use App\Domain\Borrow\ValueObject\Id;
use App\Domain\Borrow\ValueObject\ReaderId;
use App\Domain\Reader\Service\ReaderAsserter;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use DateTimeImmutable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BorrowBookHandler implements MessageHandlerInterface
{
    private AggregateRootRepositoryInterface $aggregateRootRepository;
    private BookAsserter $bookAsserter;
    private ReaderAsserter $readerAsserter;

    public function __construct(
        AggregateRootRepositoryInterface $aggregateRootRepository,
        BookAsserter $bookAsserter,
        ReaderAsserter $readerAsserter
    ) {
        $this->aggregateRootRepository = $aggregateRootRepository;
        $this->bookAsserter = $bookAsserter;
        $this->readerAsserter = $readerAsserter;
    }

    public function __invoke(BorrowBook $command): void
    {
        $this->bookAsserter->assertBookExist($command->getBookId());
        $this->readerAsserter->assertReaderExist($command->getReaderId());

        $borrow = new Borrow(
            new Id($command->getId()),
            new BookId($command->getBookId()),
            new ReaderId($command->getReaderId()),
            new DateTimeImmutable($command->getBorrowBookDate()),
            new DateTimeImmutable($command->getReturnBookDate())
        );

        $this->aggregateRootRepository->save($borrow);
    }
}