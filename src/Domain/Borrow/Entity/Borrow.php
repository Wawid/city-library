<?php

namespace App\Domain\Borrow\Entity;

use App\Domain\Borrow\Event\BookBorrowed;
use App\Domain\Borrow\ValueObject\BookId;
use App\Domain\Borrow\ValueObject\Id;
use App\Domain\Borrow\ValueObject\ReaderId;
use App\Infrastructure\Shared\Domain\Aggregate\AggregateRoot;
use App\Infrastructure\Shared\ValueObject\IdentifyInterface;
use DateTimeImmutable;

class Borrow extends AggregateRoot
{
    private Id $aggregateId;
    private BookId $bookId;
    private ReaderId $readerId;
    private DateTimeImmutable $borrowBookDate;
    private DateTimeImmutable $returnBookDate;


    public function __construct(
        Id $aggregateId,
        BookId $bookId,
        ReaderId $readerId,
        DateTimeImmutable $borrowBookDate,
        DateTimeImmutable $returnBookDate
    )
    {
        $this->record(new BookBorrowed(
            $aggregateId->getValue(),
            $bookId->getValue(),
            $readerId->getValue(),
            $borrowBookDate->format('Y-m-d'),
            $returnBookDate->format('Y-m-d')
        ));
    }

    public function applyBookBorrowed(BookBorrowed $event): void
    {
        $this->aggregateId = new Id($event->getAggregateId());
        $this->bookId = new BookId($event->getBookId());
        $this->readerId = new ReaderId($event->getReaderId());
        $this->borrowBookDate = new \DateTimeImmutable(
            $event->getBorrowBookDate(),
            new \DateTimeZone('Europe/Warsaw')
        );
        $this->returnBookDate = new \DateTimeImmutable(
            $event->getReturnBookDate(),
            new \DateTimeZone('Europe/Warsaw')
        );
    }

    public function getAggregateId(): IdentifyInterface
    {
        return $this->aggregateId;
    }

    public function getBookId(): BookId
    {
        return $this->bookId;
    }

    public function getReaderId(): ReaderId
    {
        return $this->readerId;
    }

    public function getBorrowBookDate(): DateTimeImmutable
    {
        return $this->borrowBookDate;
    }

    public function getReturnBookDate(): DateTimeImmutable
    {
        return $this->returnBookDate;
    }
}