<?php

namespace App\Domain\Borrow\Command;

class BorrowBook
{
    private string $id;
    private string $bookId;
    private string $readerId;
    private string $borrowBookDate;
    private string $returnBookDate;

    public function __construct(
        string $id,
        string $bookId,
        string $readerId,
        string $borrowBookDate,
        string $returnBookDate
    ) {
        $this->id = $id;
        $this->bookId = $bookId;
        $this->readerId = $readerId;
        $this->borrowBookDate = $borrowBookDate;
        $this->returnBookDate = $returnBookDate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getBookId(): string
    {
        return $this->bookId;
    }

    public function getReaderId(): string
    {
        return $this->readerId;
    }

    public function getBorrowBookDate(): string
    {
        return $this->borrowBookDate;
    }

    public function getReturnBookDate(): string
    {
        return $this->returnBookDate;
    }
}