<?php

namespace App\Domain\Borrow\Event;

use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class BookBorrowed implements DomainEventInterface
{
    private string $aggregateId;
    private string $bookId;
    private string $readerId;
    private string $borrowBookDate;
    private string $returnBookDate;

    public function __construct(
        string $aggregateId,
        string $bookId,
        string $readerId,
        string $borrowBookDate,
        string $returnBookDate
    ) {
        $this->aggregateId = $aggregateId;
        $this->bookId = $bookId;
        $this->readerId = $readerId;
        $this->borrowBookDate = $borrowBookDate;
        $this->returnBookDate = $returnBookDate;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getBookId(): string
    {
        return $this->bookId;
    }

    public function getReaderId(): string
    {
        return $this->readerId;
    }

    public function getBorrowBookDate(): string
    {
        return $this->borrowBookDate;
    }

    public function getReturnBookDate(): string
    {
        return $this->returnBookDate;
    }


}