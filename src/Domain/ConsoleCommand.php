<?php

namespace App\Domain;

use App\Domain\Book\Command\AddBook;
use App\Domain\Book\ReadModel\Persistence\BookRepositoryInterface;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Borrow\Command\BorrowBook;
use App\Domain\Reader\Command\AddReader;
use App\Domain\Reader\Command\ChangeReaderName;
use App\Domain\User\Command\CreateUser;
use App\Infrastructure\Persistence\Book\Doctrine\BookRepository;
use App\Infrastructure\Shared\Bus\Event\Persist\AggregateRootRepositoryInterface;
use App\Infrastructure\Shared\Bus\Event\Persist\EventStorageRepositoryInterface;
use App\Infrastructure\Shared\Bus\Message\MessageBusInterface;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleCommand extends Command
{
    protected static $defaultName = 'app:test';

    private MessageBusInterface $messageBus;
    private EventStorageRepositoryInterface $eventStorage;
    private IdentifyGeneratorInterface $identifyGenerator;
    private AggregateRootRepositoryInterface $aggregateRepository;
    private BookRepositoryInterface $bookRepository;

    public function __construct(
        MessageBusInterface $messageBus,
        EventStorageRepositoryInterface $eventStorage,
        IdentifyGeneratorInterface $identifyGenerator,
        AggregateRootRepositoryInterface $aggregateRepository,
        BookRepositoryInterface $bookRepository
    )
    {
        $this->messageBus = $messageBus;
        parent::__construct();
        $this->eventStorage = $eventStorage;
        $this->identifyGenerator = $identifyGenerator;
        $this->aggregateRepository = $aggregateRepository;
        $this->bookRepository = $bookRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('test', InputArgument::REQUIRED, 'The test of.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('start');

//        var_dump($this->bookRepository->find('a1f9740c-a9c2-42ab-9f4c-1da9b06a48b2'));

//        $this->messageBus->dispatch(new AddBook(
//            $this->identifyGenerator->generate(),
//            'test',
//            'Bob Bip',
//            BookCover::HARD(),
//            '978-83-01-00001-1',
//            155,
//            '2012-05-12'
//        ));

//        $this->messageBus->dispatch(new ChangeReaderName(
//            '6f3d0950-6a76-4e54-9303-d06d189f9c29',
//            'nowe imie po zmianie'
//        ));

        $this->messageBus->dispatch(new BorrowBook(
            $this->identifyGenerator->generate(),
            '934ae6ac-458d-41da-aa3f-442cbb0c3709',
            '6f3d0950-6a76-4e54-9303-d06d189f9c29',
            '2021-10-09',
            '2021-11-09'
        ));

//        $test = $this->aggregateRepository->find(new Id('8eab15b0-e0a4-468c-bb6a-6303d8947b33'), 'App\Domain\Book\Entity\Book');
//        var_dump($test);

//        $this->messageBus->dispatch(new CreateUser(
//            $this->identifyGenerator->generate(),
//            'test@test.pl',
//            'itijI#JIJfij',
//            ['ROLE_READER']
//        ));

        $output->writeln('end');

        return Command::SUCCESS;
    }
}