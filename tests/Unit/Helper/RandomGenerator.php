<?php

namespace App\Tests\Unit\Helper;

class RandomGenerator
{
    public function generateCharLength(int $length): string
    {
        return substr(bin2hex(random_bytes($length)), 0, $length);
    }
}