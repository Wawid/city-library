<?php

namespace App\Tests\Unit\Infrastructure\Helper;

use App\Infrastructure\Event\DomainMessage;
use App\Infrastructure\Event\DomainMessagesStream;
use App\Infrastructure\Shared\Bus\Event\DomainEventInterface;

class AggregateHelper
{
    /**
     * @return DomainEventInterface[]
     */
    public function getEvents(DomainMessagesStream $domainMessagesStream): array
    {
        return array_map(
            fn(DomainMessage $domainMessage) => $domainMessage->getPayload(), iterator_to_array($domainMessagesStream)
        );
    }
}