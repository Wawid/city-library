<?php

namespace App\Tests\Unit\Infrastructure\Shared\Generator;

use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UuidRamseyGeneratorTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::$container;

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
    }

    public function testGenerate(): void
    {
        $uuid = $this->generator->generate();
        $isValid = Uuid::isValid($uuid);

        $this->assertTrue($isValid);
    }
}