<?php

namespace App\Tests\Unit\Infrastructure\Shared\Bus\Snapshot\Factory;

use App\Domain\Book\Entity\Book;
use App\Domain\Book\Registry\IsbnRegistryInterface;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Isbn;
use App\Domain\Book\ValueObject\Name;
use App\Domain\Book\ValueObject\Page;
use App\Infrastructure\Shared\Bus\Snapshot\Factory\SnapshotFactory;
use App\Infrastructure\Shared\Bus\Snapshot\Snapshot;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SnapshotFactoryTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;
    private IsbnRegistryInterface $isbnRegistry;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::$container;

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
        $this->isbnRegistry = $container->get(IsbnRegistryInterface::class);
    }

    public function testCreateFromAggregate(): void
    {
        $book = new Book(
            new Id($this->generator->generate()),
            new Name(substr(uniqid(), 0, 6)),
            new Author(substr(uniqid(), 0, 6)),
            new BookCover('soft'),
            new Isbn('978-83-01-00001-1'),
            new Page(145),
            new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw')),
            $this->isbnRegistry
        );

        $snapshot = SnapshotFactory::createFromAggregate($book);

        $this->assertInstanceOf(Snapshot::class, $snapshot);
        $this->assertEquals(Book::class, $snapshot->getAggregateClass());
    }
}