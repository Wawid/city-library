<?php

namespace App\Tests\Unit\Infrastructure\Shared\Serializer;

use App\Domain\Book\Event\BookAdded;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Infrastructure\Shared\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JsonSerializerTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;
    private SerializerInterface $serializer;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::$container;

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
        $this->serializer = $container->get(SerializerInterface::class);
    }

    public function testSerializeData(): void
    {
        $id = $this->generator->generate();
        $name = "The sign of heaven";
        $author = "Michael Forbison";
        $cover = "hard";
        $isbn = '978-83-01-00001-1';
        $page = 14;
        $publicationDate = '2014-11-04';

        $bookCreated = new BookAdded(
            $id,
            $name,
            $author,
            $cover,
            $isbn,
            $page,
            $publicationDate
        );

        $json = sprintf(
            '{"name":"%s","aggregateId":"%s","author":"%s","bookCover":"%s","isbn":"%s","page":%d,"publicationDate":"%s"}',
            $name, $id, $author, $cover, $isbn, $page, $publicationDate
        );

        $this->assertEquals($json, $this->serializer->serialize($bookCreated));
    }

    public function testDeserializeData(): void
    {
        $id = $this->generator->generate();
        $name = "The sign of heaven";
        $author = "Michael Forbison";
        $cover = "hard";
        $isbn = '978-83-01-00001-1';
        $page = 14;
        $publicationDate = '2014-11-04';

        $bookCreated = new BookAdded(
            $id,
            $name,
            $author,
            $cover,
            $isbn,
            $page,
            $publicationDate
        );

        $json = sprintf(
            '{"name":"%s","aggregateId":"%s","author":"%s","bookCover":"%s", "isbn": "%s", "page": %d, "publicationDate": "%s"}',
            $name, $id, $author, $cover, $isbn, $page, $publicationDate
        );

        $this->assertEquals($bookCreated, $this->serializer->deserialize($json, get_class($bookCreated)));
    }
}