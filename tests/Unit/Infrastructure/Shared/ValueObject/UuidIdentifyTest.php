<?php

namespace App\Tests\Unit\Infrastructure\Shared\ValueObject;

use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Infrastructure\Shared\ValueObject\UuidIdentify;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UuidIdentifyTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::getContainer();

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
    }

    public function testCreate(): void
    {
        $this->expectNotToPerformAssertions();
        new UuidIdentify($this->generator->generate());
    }

    public function testIncorrectUuid(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new UuidIdentify('test');
    }
}