<?php

namespace App\Tests\Unit\Domain\Borrow\Entity;

use App\Domain\Book\Entity\Book;
use App\Domain\Book\Registry\IsbnRegistryInterface;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id as AggregateBookId;
use App\Domain\Book\ValueObject\Isbn;
use App\Domain\Book\ValueObject\Name;
use App\Domain\Book\ValueObject\Page;
use App\Domain\Borrow\Entity\Borrow;
use App\Domain\Borrow\Event\BookBorrowed;
use App\Domain\Borrow\ValueObject\BookId;
use App\Domain\Borrow\ValueObject\Id;
use App\Domain\Borrow\ValueObject\ReaderId;
use App\Domain\Reader\Entity\Reader;
use App\Domain\Reader\ValueObject\Email;
use App\Domain\Reader\ValueObject\Id as AggregateReaderId;
use App\Domain\Reader\ValueObject\LastName;
use App\Domain\Reader\ValueObject\Name as ReaderName;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Tests\Unit\Infrastructure\Helper\AggregateHelper;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BorrowTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;
    private IsbnRegistryInterface $isbnRegistry;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::getContainer();

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
        $this->isbnRegistry = $container->get(IsbnRegistryInterface::class);
    }

    public function testBorrowBook()
    {
        //given
        $aggregateHelper = new AggregateHelper();
        $bookId = new AggregateBookId($this->generator->generate());

        new Book(
            $bookId,
            new Name(substr(uniqid(), 0, 6)),
            new Author(substr(uniqid(), 0, 6)),
            new BookCover('soft'),
            new Isbn('978-3-16-148410-0'),
            new Page(145),
            new DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw')),
            $this->isbnRegistry
        );

        $readerId = new AggregateReaderId($this->generator->generate());
        new Reader(
            $readerId,
            new Email(substr(uniqid(), 0, 6) . '@emaple.com'),
            new ReaderName(substr(uniqid(), 0, 6)),
            new LastName(substr(uniqid(), 0, 6))
        );

        //when
        $borrowId = new Id($this->generator->generate());
        $borrowBookId = new BookId($bookId->getValue());
        $readerBookId = new ReaderId($readerId->getValue());
        $borrowBookDate = new DateTimeImmutable('2021-10-09', new \DateTimeZone('Europe/Warsaw'));
        $returnBookDate = new DateTimeImmutable('2021-11-09', new \DateTimeZone('Europe/Warsaw'));

        $borrow = new Borrow(
            $borrowId,
            $borrowBookId,
            $readerBookId,
            $borrowBookDate,
            $returnBookDate
        );

        //then
        $domainMessageStream = $borrow->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $bookBorrowed = new BookBorrowed(
            $borrowId->getValue(),
            $bookId->getValue(),
            $readerId->getValue(),
            $borrowBookDate->format('Y-m-d'),
            $returnBookDate->format('Y-m-d')
        );

        $this->assertEquals($bookBorrowed, current($events));
        $this->assertEquals($borrowId, $borrow->getAggregateId());
        $this->assertEquals($readerBookId, $borrow->getReaderId());
        $this->assertEquals($borrowBookId, $borrow->getBookId());
        $this->assertEquals($borrowBookDate, $borrow->getBorrowBookDate());
        $this->assertEquals($returnBookDate, $borrow->getReturnBookDate());
    }
}