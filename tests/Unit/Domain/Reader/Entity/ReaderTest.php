<?php

namespace App\Tests\Unit\Domain\Reader\Entity;

use App\Domain\Reader\Entity\Reader;
use App\Domain\Reader\Event\ReaderAdded;
use App\Domain\Reader\Event\ReaderEmailChanged;
use App\Domain\Reader\Event\ReaderNameChanged;
use App\Domain\Reader\ValueObject\Email;
use App\Domain\Reader\ValueObject\Id;
use App\Domain\Reader\ValueObject\LastName;
use App\Domain\Reader\ValueObject\Name;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Tests\Unit\Infrastructure\Helper\AggregateHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReaderTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::getContainer();

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
    }

    public function testCreate(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $email = new Email(substr(uniqid(), 0, 6).'@emaple.com');
        $name = new Name(substr(uniqid(), 0, 6));
        $lastName = new LastName(substr(uniqid(), 0, 6));

        // When
        $reader = new Reader($id, $email, $name, $lastName);

        // Then
        $domainMessageStream = $reader->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $readerAdded = new ReaderAdded(
            $id->getValue(),
            $email->getEmail(),
            $name->getName(),
            $lastName->getLastName()
        );

        $this->assertEquals($readerAdded, current($events));
    }

    public function testChangeName(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $email = new Email(substr(uniqid(), 0, 6).'@emaple.com');
        $lastName = new LastName(substr(uniqid(), 0, 6));

        // When
        $reader = new Reader($id, $email, new Name(substr(uniqid(), 0, 6)), $lastName);

        $reader->clearRecordedEvents();

        $newName = new Name(substr(uniqid(), 0, 6));
        $reader->changeName($newName);

        // Then
        $domainMessageStream = $reader->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $nameChanged = new ReaderNameChanged(
            $id->getValue(),
            $newName->getName()
        );

        $this->assertEquals($nameChanged, current($events));
    }

    public function testChangeEmail(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $lastName = new LastName(substr(uniqid(), 0, 6));

        // When
        $reader = new Reader($id, new Email(substr(uniqid(), 0, 6).'@emaple.com'), $name, $lastName);

        $reader->clearRecordedEvents();

        $newEmail = new Email(substr(uniqid(), 0, 6).'@emaple.com');
        $reader->changeEmail($newEmail);

        // Then
        $domainMessageStream = $reader->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $emailChanged = new ReaderEmailChanged(
            $id->getValue(),
            $newEmail->getEmail()
        );

        $this->assertEquals($emailChanged, current($events));
    }
}