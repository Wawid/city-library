<?php

namespace App\Tests\Unit\Domain\Reader\ValueObject;

use App\Domain\Reader\ValueObject\LastName;
use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;
use App\Tests\Unit\Helper\RandomGenerator;
use PHPUnit\Framework\TestCase;

class LastNameTest extends TestCase
{
    public function testTooShortLastName(): void
    {
        $this->expectException(MinValueException::class);
        new LastName('');
    }

    public function testTooLongLastName(): void
    {
        $this->expectException(MaxValueException::class);

       $randomGenerator = new RandomGenerator();
       new LastName($randomGenerator->generateCharLength(256));
    }
}