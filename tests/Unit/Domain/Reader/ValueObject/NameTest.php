<?php

namespace App\Tests\Unit\Domain\Reader\ValueObject;

use App\Domain\Reader\ValueObject\Name;
use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;
use App\Tests\Unit\Helper\RandomGenerator;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    public function testTooShortName(): void
    {
        $this->expectException(MinValueException::class);
        new Name('');
    }

    public function testTooLongName(): void
    {
        $this->expectException(MaxValueException::class);

       $randomGenerator = new RandomGenerator();
       new Name($randomGenerator->generateCharLength(256));
    }
}