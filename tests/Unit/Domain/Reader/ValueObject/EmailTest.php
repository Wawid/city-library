<?php

namespace App\Tests\Unit\Domain\Reader\ValueObject;

use App\Domain\Reader\Exception\InvalidEmailException;
use App\Domain\Reader\ValueObject\Email;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /**
     * @dataProvider invalidEmailProvider
     */
    public function testInvalidEmailAddresses(string $email): void
    {
        $this->expectException(InvalidEmailException::class);
        new Email($email);
    }

    /**
     * @return array<array<string>>
     */
    public function invalidEmailProvider(): array
    {
        return [
            ['tttt@'],
            ['test@.pl'],
            ['mar'],
            ['test@test'],
            ['']
        ];
    }
}