<?php

namespace App\Tests\Unit\Domain\Book\ValueObject;

use App\Domain\Book\ValueObject\BookCover;
use PHPUnit\Framework\TestCase;
use UnexpectedValueException;

class BookCoverTest extends TestCase
{
    public function testInvalidBookCoverType(): void
    {
        $this->expectException(UnexpectedValueException::class);

        new BookCover('tree');
    }

    public function testCreateSoftType(): void
    {
        // checks that an exception has not been thrown
        $this->expectNotToPerformAssertions();

        new BookCover('soft');
    }

    public function testCreateHardType(): void
    {
        // checks that an exception has not been thrown
        $this->expectNotToPerformAssertions();

        new BookCover('hard');
    }
}