<?php

namespace App\Tests\Unit\Domain\Book\ValueObject;

use App\Domain\Book\Exception\IncorrectIsbnException;
use App\Domain\Book\ValueObject\Isbn;
use PHPUnit\Framework\TestCase;

class IsbnTest extends TestCase
{
    /**
     * @dataProvider isbnProvider
     */
    public function testCreateIsbn(string $isbn): void
    {
        // checks that an exception has not been thrown
        $this->expectNotToPerformAssertions();

        new Isbn($isbn);
    }

    /**
     * @return string[][]
     */
    public function isbnProvider(): array
    {
        return [
            ['978-83-01-00001-1'],
            ['0-545-01022-5'],
            ['9782123456803']
        ];
    }

    /**
     * @dataProvider incorrectIsbnProvider
     */
    public function testIncorrectIsbn(string $isbn): void
    {
        $this->expectException(IncorrectIsbnException::class);

        new Isbn($isbn);
    }

    /**
     * @return string[][]
     */
    public function incorrectIsbnProvider(): array
    {
        return [
            ['aaaa'],
            ['1-3984-232-1-2'],
            ['978-121-222-444-1'],
            ['12345']
        ];
    }
}