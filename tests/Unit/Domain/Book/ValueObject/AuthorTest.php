<?php

namespace App\Tests\Unit\Domain\Book\ValueObject;

use App\Domain\Book\ValueObject\Author;
use App\Domain\Shared\Exception\MaxValueException;
use App\Domain\Shared\Exception\MinValueException;
use App\Tests\Unit\Helper\RandomGenerator;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testTooShortAuthor(): void
    {
        $this->expectException(MinValueException::class);
        new Author('');
    }

    public function testTooLongAuthor(): void
    {
        $this->expectException(MaxValueException::class);

       $randomGenerator = new RandomGenerator();
       new Author($randomGenerator->generateCharLength(256));
    }
}