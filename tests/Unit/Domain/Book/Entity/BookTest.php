<?php

namespace App\Tests\Unit\Domain\Book\Entity;

use App\Domain\Book\Entity\Book;
use App\Domain\Book\Event\BookAdded;
use App\Domain\Book\Event\BookAuthorChanged;
use App\Domain\Book\Event\BookCoverChanged;
use App\Domain\Book\Event\BookIsbnChanged;
use App\Domain\Book\Event\BookNameChanged;
use App\Domain\Book\Event\BookPageChanged;
use App\Domain\Book\Event\BookPublicationDateChanged;
use App\Domain\Book\Exception\UniqueIsbnRequireException;
use App\Domain\Book\Registry\Entity\IsbnRegistry;
use App\Domain\Book\Registry\IsbnRegistryInterface;
use App\Domain\Book\Registry\Persistence\IsbnRegistryRepositoryInterface;
use App\Domain\Book\ValueObject\Author;
use App\Domain\Book\ValueObject\BookCover;
use App\Domain\Book\ValueObject\Id;
use App\Domain\Book\ValueObject\Isbn;
use App\Domain\Book\ValueObject\Name;
use App\Domain\Book\ValueObject\Page;
use App\Infrastructure\Shared\Generator\IdentifyGeneratorInterface;
use App\Tests\Unit\Infrastructure\Helper\AggregateHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookTest extends KernelTestCase
{
    private IdentifyGeneratorInterface $generator;
    private IsbnRegistryInterface $isbnRegistry;
    private IsbnRegistryRepositoryInterface $isbnRegistryRepository;

    public function setUp(): void
    {
        self::bootKernel();
        $container = self::getContainer();

        $this->generator = $container->get(IdentifyGeneratorInterface::class);
        $this->isbnRegistry = $container->get(IsbnRegistryInterface::class);
        $this->isbnRegistryRepository = $container->get(IsbnRegistryRepositoryInterface::class);
    }

    public function testCreate(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $isbn = new Isbn('978-83-01-00001-1');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        // when
        $book = new Book($id, $name, $author, $bookCover, $isbn, $page, $publicationDate, $this->isbnRegistry);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $bookCreated = new BookAdded(
                $id->getValue(),
                $name->getName(),
                $author->getAuthor(),
                $bookCover->getValue(),
                $isbn->getIsbn(),
                $page->getPage(),
                $publicationDate->format('Y-m-d')
            );

        $this->assertEquals($bookCreated, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testCreateWithTheSameIsbn(): void
    {
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $isbn = new Isbn('9782123456803');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        $this->isbnRegistryRepository->save(new IsbnRegistry($id->getValue(), $isbn->getIsbn()));

        $this->expectException(UniqueIsbnRequireException::class);

        // when
        new Book($id, $name, $author, $bookCover, $isbn, $page, $publicationDate, $this->isbnRegistry);
    }

    public function testChangeName(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $isbn = new Isbn('978-83-01-00001-1');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        // given
        $book = new Book(
            $id,
            new Name(substr(uniqid(), 0, 6)),
            $author,
            $bookCover,
            $isbn,
            $page,
            $publicationDate,
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newName = new Name(substr(uniqid(), 0, 6));

        // when
        $book->changeName($newName);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $nameChanged = new BookNameChanged($id->getValue(), $newName->getName());

        $this->assertEquals($nameChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($newName, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testChangeAuthor(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $isbn = new Isbn('978-83-01-00001-1');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        // given
        $book = new Book(
            $id,
            $name,
            new Author(substr(uniqid(), 0, 6)),
            $bookCover,
            $isbn,
            $page,
            $publicationDate,
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newAuthor = new Author(substr(uniqid(), 0, 6));

        // when
        $book->changeAuthor($newAuthor);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $authorChanged = new BookAuthorChanged($id->getValue(), $newAuthor->getAuthor());

        $this->assertEquals($authorChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($newAuthor, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testChangeCover(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $isbn = new Isbn('978-83-01-00001-1');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        // given
        $book = new Book(
            $id,
            $name,
            $author,
            new BookCover('soft'),
            $isbn,
            $page,
            $publicationDate,
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newCover = new BookCover('hard');

        // when
        $book->changeBookCover($newCover);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $coverChanged = new BookCoverChanged($id->getValue(), $newCover->getValue());

        $this->assertEquals($coverChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($newCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testChangeISBN(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $page = new Page(145);
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));

        // given
        $book = new Book(
            $id,
            $name,
            $author,
            $bookCover,
            new Isbn('978-83-01-00001-1'),
            $page,
            $publicationDate,
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newIsbn = new Isbn('83-7361-936-4');

        // when
        $book->changeIsbn($newIsbn);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $coverChanged = new BookIsbnChanged($id->getValue(), $newIsbn->getIsbn());

        $this->assertEquals($coverChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($newIsbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testChangePage(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $publicationDate = new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw'));
        $isbn = new Isbn('978-83-01-00001-1');

        // given
        $book = new Book(
            $id,
            $name,
            $author,
            $bookCover,
            $isbn,
            new Page(145),
            $publicationDate,
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newPage = new Page(648);

        // when
        $book->changePage($newPage);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $coverChanged = new BookPageChanged($id->getValue(), $newPage->getPage());

        $this->assertEquals($coverChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($newPage, $book->getPage());
        $this->assertEquals($publicationDate, $book->getPublicationDate());
    }

    public function testChangePublicationDate(): void
    {
        $aggregateHelper = new AggregateHelper();
        $id = new Id($this->generator->generate());
        $name = new Name(substr(uniqid(), 0, 6));
        $author = new Author(substr(uniqid(), 0, 6));
        $bookCover = new BookCover('soft');
        $page = new Page(145);
        $isbn = new Isbn('978-83-01-00001-1');

        // given
        $book = new Book(
            $id,
            $name,
            $author,
            $bookCover,
            $isbn,
            $page,
            new \DateTimeImmutable('2014-11-04', new \DateTimeZone('Europe/Warsaw')),
            $this->isbnRegistry
        );

        $book->clearRecordedEvents();

        $newPublicationDate = new \DateTimeImmutable(
            '2018-04-16'
        );

        // when
        $book->changePublicationDate($newPublicationDate);

        // then
        $domainMessageStream = $book->pullDomainEvents();
        $events = $aggregateHelper->getEvents($domainMessageStream);

        $coverChanged = new BookPublicationDateChanged($id->getValue(), $newPublicationDate->format('Y-m-d'));

        $this->assertEquals($coverChanged, current($events));
        $this->assertEquals($id, $book->getAggregateId());
        $this->assertEquals($name, $book->getName());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($bookCover, $book->getBookCover());
        $this->assertEquals($isbn, $book->getIsbn());
        $this->assertEquals($page, $book->getPage());
        $this->assertEquals($newPublicationDate, $book->getPublicationDate());
    }
}